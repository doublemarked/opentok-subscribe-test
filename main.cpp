#include <fstream>
#include <iostream>
#include <map>

#include <opentok.h>
#include <uv.h>

constexpr otc_log_level OTC_LOGLEVEL = OTC_LOG_LEVEL_WARN;

// Our anticipated stream config
constexpr int EXPECTED_RATE = 48000;
constexpr size_t EXPECTED_CHANNELS = 2;

#define SAMPLE_RATE 48000
#define NUM_CHANNELS 2
#define FRAME_SIZE_MS 10
#define NUM_SAMPLES_PER_FRAME (SAMPLE_RATE / 1000 * FRAME_SIZE_MS)

extern void default_otc_logger_callback(const char *message);
extern struct otc_audio_device_callbacks default_device_callbacks;
extern struct otc_session_callbacks default_session_callbacks;
extern struct otc_subscriber_callbacks default_subscriber_callbacks;

struct subscriber_context {
    std::string id;
    otc_subscriber *subscriber;
    std::string stream_id;
    std::ofstream fh;
    int64_t started = 0;
    int total_samples = 0;
};

struct device_context {
    uv_thread_t renderer_thread;
    std::atomic<bool> exiting;
};

uv_loop_t *loop;
uv_signal_t sig_int;

std::map<std::string, subscriber_context *> subscribers;
std::string g_outputDir;

inline int64_t now_ms() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

static void *device_renderer_thread_fn(void *arg) {
    std::cout << "[audio_device.device_renderer_thread_fn]" << std::endl;

    struct device_context *ctx = static_cast<struct device_context *>(arg);

    std::ofstream fh(g_outputDir + "/device.pcm", std::ios::trunc | std::ios::binary);

    while (ctx->exiting.load() == false) {
        int16_t samples[NUM_SAMPLES_PER_FRAME * NUM_CHANNELS];

        size_t actual = otc_audio_device_read_render_data(samples, NUM_SAMPLES_PER_FRAME);

        std::cout << "Read samples: " << actual << " writing bytes: " << actual * NUM_CHANNELS * sizeof(int16_t) << std::endl;

        if (actual > 0) {
            fh.write(reinterpret_cast<const char *>(samples), actual * NUM_CHANNELS * sizeof(int16_t));
        }

        usleep(FRAME_SIZE_MS * 1000);
    }

    fh.close();

    std::cout << "[audio_device.device_renderer_thread_fn] exiting" << std::endl;

    return nullptr;
}

static otc_bool audio_device_start_renderer(const otc_audio_device *audio_device, void *user_data) {
    std::cout << "[audio_device.start_renderer]" << std::endl;

    struct device_context *ctx = static_cast<struct device_context *>(user_data);
    ctx->exiting = false;

    if (pthread_create(&(ctx->renderer_thread), NULL, &device_renderer_thread_fn, ctx) != 0) {
        return OTC_FALSE;
    }

    return OTC_TRUE;
}

static otc_bool audio_device_destroy_renderer(const otc_audio_device *audio_device, void *user_data) {
    std::cout << "[audio_device.destroy_renderer]" << std::endl;
    struct device_context *ctx = static_cast<struct device_context *>(user_data);

    ctx->exiting = true;
    pthread_join(ctx->renderer_thread, NULL);

    return OTC_TRUE;
}

static otc_bool get_render_settings(const otc_audio_device *audio_device, void *user_data, struct otc_audio_device_settings *settings) {
    std::cout << "[audio_device.get_render_settings]" << std::endl;
    if (settings == nullptr)
        return OTC_FALSE;

    settings->number_of_channels = NUM_CHANNELS;
    settings->sampling_rate = SAMPLE_RATE;

    return OTC_TRUE;
}

static void on_subscriber_connected(otc_subscriber *subscriber, void *user_data, const otc_stream *stream) {
    auto *ctx = static_cast<struct subscriber_context *>(user_data);
    std::cout << "[subscriber.on_connected] " << ctx->id << std::endl;

    otc_subscriber_set_subscribe_to_video(subscriber, OTC_FALSE);
    otc_subscriber_set_subscribe_to_audio(subscriber, OTC_TRUE);

    ctx->fh.open(g_outputDir + "/" + ctx->id + ".pcm", std::ios::binary);
}

static void on_subscriber_disconnected(otc_subscriber *subscriber, void *user_data) {
    auto *ctx = static_cast<struct subscriber_context *>(user_data);
    std::cout << "[subscriber.on_disconnected] " << ctx->id << std::endl;

    if (ctx->fh.is_open()) {
        ctx->fh.flush();
    }
}

static void on_subscriber_audio_data(otc_subscriber *subscriber,
                                     void *user_data,
                                     const struct otc_audio_data *audio_data) {
    auto *ctx = static_cast<struct subscriber_context *>(user_data);

    // We are generally unopinionated about the sample rate, however, the fluctuations that can occur
    // in response to network behavior interfer with our test and we prefer to not deal w/ resampling
    // right now. Generally this only happens at the start of the stream.
    if (audio_data->sample_rate != EXPECTED_RATE || audio_data->number_of_channels != EXPECTED_CHANNELS) {
        printf("[%s] DROPPING unexpected frame: sample rate %dHz / %ld channels\n",
               ctx->id.c_str(),
               audio_data->sample_rate,
               audio_data->number_of_channels);
        return;
    }

    if (ctx->started == 0) {
        ctx->started = now_ms();
    }

    size_t bytes = audio_data->number_of_samples * (audio_data->bits_per_sample >> 3) * audio_data->number_of_channels;
    auto seconds = (now_ms() - ctx->started) / 1000.0;

    printf("[%s] frame: %ld samples, %dHz, %ld channels; %d total samples, %.2fs, %d samples/second\n",
           ctx->id.c_str(),
           audio_data->number_of_samples,
           audio_data->sample_rate,
           audio_data->number_of_channels,
           ctx->total_samples,
           seconds,
           static_cast<int>(ctx->total_samples / seconds));

    ctx->total_samples += audio_data->number_of_samples;
    ctx->fh.write(reinterpret_cast<const char *>(audio_data->sample_buffer), bytes);
}

struct otc_subscriber_callbacks subscriber_callbacks = default_subscriber_callbacks;

static void on_session_stream_received(otc_session *session,
                                       void *user_data,
                                       const otc_stream *stream) {
    const char *stream_id = otc_stream_get_id(stream);
    std::cout << "[session.on_stream_received]: " << stream_id << std::endl;

    struct subscriber_context *ctx = new struct subscriber_context();
    ctx->stream_id = stream_id;

    subscriber_callbacks.on_connected = on_subscriber_connected;
    subscriber_callbacks.on_disconnected = on_subscriber_disconnected;
    subscriber_callbacks.on_audio_data = on_subscriber_audio_data;

    subscriber_callbacks.user_data = static_cast<void *>(ctx);

    otc_subscriber *subscriber = otc_subscriber_new(stream, &subscriber_callbacks);

    if (subscriber == nullptr) {
        std::cerr << "[session.on_stream_received]: " << stream_id << " otc_subscriber_new() failed" << std::endl;
        delete ctx;
        return;
    }
    if (otc_session_subscribe(session, subscriber) != OTC_SUCCESS) {
        std::cerr << "[session.on_stream_received]: " << stream_id << " otc_session_subscribe() failed" << std::endl;
        delete ctx;
        return;
    }

    ctx->subscriber = subscriber;
    ctx->id = otc_subscriber_get_subscriber_id(subscriber);
    subscribers[ctx->id] = ctx;
    std::cout << "[session.on_stream_received]: " << ctx->id << " SUBSCRIBED" << std::endl;
}

void abort(uv_signal_t *handle, int signum) {
    uv_signal_stop(handle);
    uv_stop(loop);
}

int main(int argc, char **argv) {
    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << "<output dir> <apiKey> <sessionId> <token>" << std::endl;
        return EXIT_FAILURE;
    }

    g_outputDir = argv[1];
    const char *apiKey = argv[2];
    const char *sessionId = argv[3];
    const char *token = argv[4];

    std::cout << "OUTPUT DIR: " << g_outputDir << std::endl;
    std::cout << "API KEY: " << apiKey << std::endl;
    std::cout << "SESSION ID: " << sessionId << std::endl;
    std::cout << "TOKEN: " << token << std::endl;

    if (otc_init(nullptr) != OTC_SUCCESS) {
        std::cerr << "otc_init() failed" << std::endl;
        return EXIT_FAILURE;
    }

    otc_log_set_logger_callback(default_otc_logger_callback);
    otc_log_enable(OTC_LOGLEVEL);

    struct device_context device_ctx = {0};
    struct otc_audio_device_callbacks device_callbacks = default_device_callbacks;
    device_callbacks.get_render_settings = get_render_settings;
    device_callbacks.start_renderer = audio_device_start_renderer;
    device_callbacks.destroy_renderer = audio_device_destroy_renderer;
    device_callbacks.user_data = &device_ctx;

    otc_set_audio_device(&device_callbacks);

    struct otc_session_callbacks session_callbacks = default_session_callbacks;
    session_callbacks.on_stream_received = on_session_stream_received;

    otc_session *session = otc_session_new(apiKey, sessionId, &session_callbacks);

    if (session == nullptr) {
        std::cerr << "otc_session_new() failed" << std::endl;
        return EXIT_FAILURE;
    }

    otc_session_connect(session, token);

    loop = uv_default_loop();
    uv_signal_init(loop, &sig_int);
    uv_signal_start(&sig_int, abort, SIGINT);
    uv_run(loop, UV_RUN_DEFAULT);

    std::cout << "Aborting due to SIGINT" << std::endl;
    uv_loop_close(loop);

    otc_session_disconnect(session);
    otc_session_delete(session);
    otc_destroy();

    for (auto &pair : subscribers) {
        struct subscriber_context *ctx = pair.second;

        if (ctx->fh.is_open()) {
            ctx->fh.close();
        }

        otc_subscriber_delete(ctx->subscriber);
        delete ctx;
    }

    subscribers.clear();

    return EXIT_SUCCESS;
}
