# Simple Sample Subscriber

This is a simple project distilled to do the following:
1. Connect to a session.
2. Subscribe to audio of all streams.
3. Use the otc_subscriber_callbacks.on_audio_data callback to receive subscriber-specific audio samples.
4. Write these samples into raw PCM files in the output/ folder.

This project is designed to run in a Docker container which lacks true audio devices. It creates a dummy audio 
device using Pulse Audio. There is another branch that instead uses a custom audio device with otc_set_audio_device().

Please note: We are aware that this specific sample could be built with the Audio Connector. This is extracted from a larger more complex project that needs functionality beyond what the Audio Connector provides.

# Some Questions

1. We're having trouble with the samples produced, which don't match source audio. We have tried some of the sample projects (e.g. https://github.com/nexmo-se/opentok-alsa-out-via-subscriber-audio-data) and had trouble to get those working properly as well.
2. It seems that the sample rate and channels can change frame by frame, and that we will need to implement some kind of dynamic resampling to deal with this. Is that what you recommend? Is there any way to stabilize the audio sample rate? 

# Setup

Build the docker image: 
```
docker build -t opentok-subscribe-test .
```

# Usage

1. Go to https://tokbox.com/developer/tools/playground/ and create a new session and token
2. Copy your API Key, Session ID, and Token values
3. Publish a new stream that includes audio
4. Execute the following command, making sure to fill in your API Key, Session ID, and Token:
    `docker run -it --platform=linux/amd64 -v $(pwd):/app opentok-subscribe-test output/ <api-key> <session-id> <token>`
6. Generate audio in the playground.
6. Observe output.
7. Take resulting PCM files from `output/` and analyze them.

# Other commands

Build without running:
```
    docker run --platform=linux/amd64 -v $(pwd):/app --entrypoint=scripts/build.sh opentok-subscribe-test
```

Convert output/ dir PCMs to MP3:
```
    docker run --platform=linux/amd64 -v $(pwd):/app --entrypoint=scripts/convert-pcm.sh opentok-subscribe-test
```
