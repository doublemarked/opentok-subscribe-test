#!/bin/sh

set -e

SCRIPTDIR=$(dirname "$0")
BUILDDIR=$(realpath --relative-to=. $SCRIPTDIR/../build)
OUTPUTDIR=$(realpath --relative-to=. $SCRIPTDIR/../output)

mkdir -p $OUTPUTDIR

set -x

pulseaudio --daemonize --exit-idle-time=-1 --log-level=error

$BUILDDIR/opentok-subscriber-example $@

