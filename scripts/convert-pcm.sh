#!/bin/sh

# Convenience script for converting output files from PCM to MP3

set -e

# Config
RATE=48000
CHANNELS=2
FORMAT=s16le

SCRIPTDIR=$(dirname "$0")
OUTPUTDIR=$(realpath --relative-to=. $SCRIPTDIR/../output)

FORCE=false
if [ "$1" = "--force" ]; then
    FORCE=true
    shift
fi

if [ "$#" -gt 0 ]; then
    FILES="$@"
    FORCE=true
else
    FILES="$OUTPUTDIR/*.pcm"
fi

echo "Converting PCM files to MP3 ("$RATE"Hz, $CHANNELS channels, $FORMAT)"

for f in $FILES; do 
    of="$OUTPUTDIR/$(basename $f .pcm).mp3"
    if [ "$FORCE" = true ] || [ ! -f "$of" ]; then
        ffmpeg -y -loglevel error -f $FORMAT -ar $RATE -ac $CHANNELS -i $f $of
        echo "   $f => $of"
    fi
done
