#!/bin/sh

set -e

SCRIPTDIR=$(dirname "$0")
BUILDDIR=$(realpath --relative-to=. $SCRIPTDIR/../build)

mkdir -p $BUILDDIR
cd $BUILDDIR
CC=clang CXX=clang++ cmake ..
make
