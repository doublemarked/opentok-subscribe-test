#!/bin/sh

set -e

SCRIPTDIR=$(dirname "$0")

$SCRIPTDIR/build.sh
$SCRIPTDIR/run.sh $@