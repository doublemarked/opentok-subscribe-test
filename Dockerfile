FROM --platform=linux/amd64 debian:bookworm

RUN echo "deb http://security.debian.org/ bookworm-security main" | tee -a /etc/apt/sources.list

RUN apt-get update \
    && apt-get -yqqf --no-install-recommends install \
       ca-certificates \
       wget \
       gnupg \
       curl \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN curl -s https://packagecloud.io/install/repositories/tokbox/debian/script.deb.sh | bash

RUN apt-get update \
  && apt-get install -y \
     software-properties-common \
     build-essential \
     pkg-config \
     cmake \
     clang \
     libc++-dev \
     libc++abi-dev \
     libopentok-dev \
     pulseaudio ffmpeg \
     libuv1-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY dummy.pa /etc/pulse/default.pa.d/

VOLUME /app
WORKDIR /app

ENTRYPOINT ["scripts/build-and-run.sh"]